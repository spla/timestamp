import sys
import datetime as dt
from datetime import datetime, timezone, timedelta
import time

def get_timestamp(date):

    date = dt.datetime.strptime(date, '%d-%m-%Y')

    return int(dt.datetime(date.year, date.month, date.day, 0, 0).timestamp())

if __name__ == '__main__':

    if len(sys.argv) == 2:

        result = utc_timestamp_date = get_timestamp(sys.argv[1])

        print(f'first timestamp of {sys.argv[1]} is {result}')

    else:

        print('add a date as argument. Ex.: python timestamp.py 13-4-2022')
