# timestamp
timestamp gets the first timestamp of the entered date.  

### Dependencies

-   **Python 3**

### Usage:

1. Clone this repo: `git clone https://gitlab.com/spla/timestamp.git <target dir>`  

2. cd into your `<target dir>` and create the Python Virtual Environment: `python3.x -m venv .`  
 
3. Activate the Python Virtual Environment: `source bin/activate`  
  
4. Run `python timestamp.py <date>` to get the first timestamp of that day..  
